/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import RNFetchBlob from 'react-native-fetch-blob'
import {
    Platform,
    StyleSheet,
    WebView,
    Linking,
    View
} from 'react-native';
import {handleUpload} from "./src/handleUpload";
import {handleOpenURL} from "./src/handleOpenURL";

// const serverURL = "http://10.0.2.2:8080/";
const serverURL = "https://test-rem.herokuapp.com/";
const uploadURL = `${serverURL}upload`;

type Props = {};
export default class App extends Component<Props> {

    constructor(props) {
        super(props);
        this.webview = React.createRef();
    }

    handleMesg = async (e) => {
        debugger
        let data;

        try {
            data = JSON.parse(e.nativeEvent.data);
        } catch (err) {
            data = e.nativeEvent.data
        }

        if (data === "upload") {
            try {
                const uploadStats = await handleUpload(uploadURL);
                this.renderLinkIntoWebView(uploadStats.filename);

            } catch (e) {
                console.error(e);
            }
        }
        if (data.url) {
            await handleOpenURL(data.url);
        }
    };

    renderLinkIntoWebView = (url) => {
        this.webview.current.postMessage(url);
    };

    render() {
        return (
            <View style={{
                flexDirection: 'row',
                top: 0,
                left: 0,
                right: 0,
                height: 500,
                padding: 0,
            }}>
                <WebView
                    testID="webView"
                    ref={this.webview}
                    source={{uri: serverURL}}
                    style={{marginTop: 0}}
                    onMessage={this.handleMesg}
                />
                {
                    process.env.NODE_ENV === "test" && <div>Testing</div>
                }
            </View>
        );
    }
}