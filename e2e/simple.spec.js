import { expect as jestExpect } from "jest"

// Detox is very limited, this is as much as i can do for a interview test

describe('Example', () => {

    it('should have webview loaded', async () => {
        await expect(element(by.id('webView'))).toBeVisible();
    });

    it('should behave correctly with upload function', async () => {
        // Use simple hack, instead of briging from webview to native control
        await element(by.id('webView')).tapAtPoint({x: 10, y: 10});
        await waitFor(element(by.id('none'))).toExist().withTimeout(200);
        await element(by.id('webView')).tapAtPoint({x: 10, y: 40});
        await expect(element(by.text("test.openURL.ok"))).toBeVisible();
    });

});