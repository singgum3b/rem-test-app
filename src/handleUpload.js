import RNFetchBlob from "react-native-fetch-blob";

export async function handleUpload(uploadURL) {
    const files =  await RNFetchBlob.android.getContentIntent();
    const stats = await RNFetchBlob.fs.stat(files);
    const res = await RNFetchBlob.fetch('POST', uploadURL, {
        'Content-Type' : 'multipart/form-data'
    }, [
        { name : "upload", filename : stats.filename, data : RNFetchBlob.wrap(files) },
    ]);

    return uploadStats = await res.json();
}