import {Linking} from "react-native";

export async function handleOpenURL(url) {
    if (await Linking.canOpenURL(url)) {
        Linking.openURL(url);
    }
}